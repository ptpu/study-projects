<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string title
 * @property string|null description
 * @property int id
 * @property int semester_id
 */
class Project extends Model
{
    public function semester(){
        return $this->belongsTo(Semester::class);
    }
    public function owner(){
        return $this->belongsTo(User::class);
    }
    public function users(){
        return $this->belongsToMany(User::class, 'project_user');
    }
    public function antworten(){
        return $this->hasMany(Antwort::class);
    }
    public function checkAntworten(){
        return $this->hasMany(CheckAntwort::class);
    }
}
