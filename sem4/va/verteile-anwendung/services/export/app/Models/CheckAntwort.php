<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CheckAntwort extends Model
{
    public function checkliste(){
        return $this->belongsTo(ChecklisteItem::class);
    }
    public function project(){
        return $this->belongsTo(Project::class);
    }
}
