<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\AdminController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::get('lang/{locale}')->name("locale");  // unused, but needed for route definition in templates

Route::middleware(['auth'])->middleware(['admin'])->group(function () {


    Route::get('/export/project/{project}')->name('export');  // unused, but needed for route definition in templates


    Route::get('/admin', 'HomeController@index')->name('home');  // TODO remove user stuff


    Route::get('/admin/semester/add', [AdminController::class, 'get_addSemester'])
        ->name('admin.semester.add');
    Route::post('/admin/semester/add', [AdminController::class, 'createSemester'])
        ->name('admin.semester.add');
    Route::delete('/admin/semester/{semester}/delete', [AdminController::class, 'deleteSemester'])
        ->name('admin.semester.delete');


    Route::get('/admin/project/add', [AdminController::class, 'get_addProject'])
        ->name('admin.project.add');
    Route::post('/admin/project/add', [AdminController::class, 'createProject'])
        ->name('admin.project.add');
    Route::get('/admin/projects/edit', [HomeController::class, 'listJoinProjects'])
        ->name('admin.projects.edit');
    Route::get('/admin/project/{project}/delete', [AdminController::class, 'deleteProject'])
        ->name('admin.project.delete');
    Route::get('/admin/project/{project}/edit', [AdminController::class, 'viewEditProject'])
        ->name('admin.project.edit');
    Route::post('/admin/project/{project}/edit', [AdminController::class, 'editProject'])
        ->name('admin.project.edit');


    Route::put('/admin/document/updateAll', [AdminController::class, 'updateTasksOrder']);
    Route::get('/admin/document/add', [AdminController::class, 'get_addDocument'])
        ->name('admin.document.add');
    Route::get('/admin/document/{document}/delete', [AdminController::class, 'deleteDocument'])
        ->name('admin.document.delete');
    Route::post('/admin/document/{document}/edit/title', [AdminController::class, 'editDocumentTitle'])
        ->name('admin.document.edit_title');

    Route::post('/admin/document/add', [AdminController::class, 'addDocument'])
        ->name('admin.document.add');
    Route::get('/admin/documents/list', [AdminController::class, 'listDocuments'])
        ->name('admin.documents.list');
    Route::get('/admin/document/{document}/details', [AdminController::class, 'details_documents'])
        ->name('admin.document.details');
    Route::post('/admin/document/{document}/frage/add', [AdminController::class, 'addFrage'])
        ->name('admin.frage.add');
    Route::post('/admin/document/{document}/frage/{frage}/edit', [AdminController::class, 'editFrage'])
        ->name('admin.frage.edit');

    Route::get('/admin/frage/{frage}/delete', [AdminController::class, 'deleteFrage'])
        ->name('admin.frage.delete');

    Route::get('admin/frage/{frage}/level', [AdminController::class, 'levelFrage'])
        ->name('admin.frage.level');


    Route::post('/admin/document/{document}/check/add', [AdminController::class, 'addCheck'])
        ->name('admin.check.add');
    Route::post('/admin/document/{document}/check/{check_item}/edit', [AdminController::class, 'editCheck'])
        ->name('admin.check.edit');
    Route::get('/admin/check/{checkitem}/delete', [AdminController::class, 'deleteCheck'])
        ->name('admin.check.delete');

});

Auth::routes();  // unused, but needed for route definition in templates
