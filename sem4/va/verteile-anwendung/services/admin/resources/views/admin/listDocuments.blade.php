@extends('layouts.app')
<?php
    /** @var \App\Models\Document[] $documents */
?>
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __("Documents") }}</div>
                        <div class="card-body">


                                    <task-draggable :docs="{{ $documents }}" ></task-draggable>
                                <!-- -->


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
