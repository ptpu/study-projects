@extends('layouts.app')
<?php
    /** @var \App\Models\Document $document */
    /** @var \App\Models\Frage[] $fragen */
?>
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div id="accordion-doc">
                        <div class="card-header" id="doc-edit-collapse">
                            <button  class="btn btn-link"  data-toggle="collapse" data-target="#doc-collapse-target">{{ __("Edit Document") }}</button>
                        </div>
                        <div class="card-body collapse" id="doc-collapse-target">
                            <form method="POST" action="{{ route('admin.document.edit_title', ['document' => $document]) }}">
                                @csrf

                                <div class="form-group">
                                    <label for="title_de">{{ __("Title_de") }}</label>
                                    <input type="text" class="form-control" id="title_de" name="title_de" required value="{{$document->title_de}}">
                                    <small class="form-text text-muted">{{ __("Explanation Document Title DE") }}</small>
                                </div>
                                <div class="form-group">
                                    <label for="title_en">{{ __("Title_en") }}</label>
                                    <input type="text" class="form-control" id="title_en" name="title_en" required value="{{$document->title_en}}">
                                    <small class="form-text text-muted">{{ __("Explanation Document Title EN") }}</small>
                                </div>
                                <div class="form-group">
                                    <label for="exclude">{{ __("Exclude from Export") }}</label>
                                    <select class="form-control" id="exclude" name="exclude_from_export">
                                        <option value="1" @if($document->exclude_from_export) selected @endif >{{ __("Exclude from Export") }}</option>
                                        <option value="0" @if(!$document->exclude_from_export) selected @endif >{{ __("Exclude in Export") }}</option>
                                    </select>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-primary">{{ __("Save") }}</button>
                                <a href="{{route('admin.document.delete', ['document' => $document])}}" class="btn btn-danger" onclick="return confirm({{ __("Do you really want to delete this document?") }})">{{ __("Delete") }}</a>
                            </form>
                        </div>
                    </div>
                </div>
                <br><br>

                <div class="card">
                    <div class="card-header">{{ __("Document: ") }}{{$document->title}}</div>
                        <div class="card-body">
                            <ul class="list-group">
                                <div id="accordion">
                                    @foreach($fragen as $index => $f)
                                        <div class="card level-{{$f->level}}">
                                            <div class="card-header" id="frage{{$index}}Collapse">
                                                <h5 class="mb-0">
                                                    <button class="btn btn-link" data-toggle="collapse" data-target="#frage{{$index}}">
                                                        {{$f->get_title()}}
                                                    </button>
                                                    <span class="float-right">
{{--                                                    <a href="">UP</a>--}}
{{--                                                    <a href="">DOWN</a>--}}
                                                    <a title="{{ __("Reverse Indention") }}" href="{{route('admin.frage.level', ['frage' => $f, 'direction' => 'left'])}}"><i class="fas fa-chevron-left"></i></a>
                                                    <a title="{{ __("Indention") }}" href="{{route('admin.frage.level', ['frage' => $f, 'direction' => 'right'])}}"><i class="fas fa-chevron-right"></i></a>
                                                    </span>
                                                </h5>
                                            </div>
                                            <div id="frage{{$index}}" class="collapse" data-parent="#accordion">
                                                <div class="card-body">

                                                    <form method="POST" action="{{ route('admin.frage.edit', ['document' => $document, 'frage' => $f]) }}">
                                                        @csrf
                                                        <div class="form-group">
                                                            <label for="title_de">{{ __("Title_de") }}</label>
                                                            <input type="text" class="form-control" id="title_de" name="title_de" required value="{{$f->title_de}}">
                                                            <small class="form-text text-muted">{{ __("Explanation Question Title DE") }}</small>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="beschreibung_de">{{ __("Description_de") }}</label>
                                                            <textarea class="form-control tinymce" id="beschreibung_de" rows="3" name="description_de">{{$f->description_de}}</textarea>
                                                            <small class="form-text text-muted">{{ __("Explanation Question Description DE") }}</small>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="title_en">{{ __("Title_en") }}</label>
                                                            <input type="text" class="form-control" id="title_en" name="title_en" required value="{{$f->title_en}}">
                                                            <small class="form-text text-muted">{{ __("Explanation Question Title EN") }}</small>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="beschreibung_en">{{ __("Description_en") }}</label>
                                                            <textarea class="form-control tinymce" id="beschreibung_en" rows="3" name="description_en">{{$f->description_en}}</textarea>
                                                            <small class="form-text text-muted">{{ __("Explanation Question Description EN") }}</small>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="frageType">{{ __("Type of Question") }}</label>
                                                            <select class="form-control" id="frageType" name="type">
                                                                <option value="{{\App\Models\Frage::TEXT}}" @if($f->type == \App\Models\Frage::TEXT) selected @endif>
                                                                    {{ __("Text") }}</option>
                                                                <option value="{{\App\Models\Frage::TITEL}}" @if($f->type == \App\Models\Frage::TITEL) selected @endif>
                                                                    {{  __("Information/Headline") }}</option>
                                                            </select>
                                                            <small class="form-text text-muted">
                                                                {{ __("Explanation Question Text") }}
                                                                <br>
                                                                {{ __("Explanation Question Information/Headline") }}
                                                            </small>
                                                        </div>
                                                        <br>
                                                        <button type="submit" class="btn btn-primary">{{ __("Save") }}</button>
                                                        <a href="{{route('admin.frage.delete', ['frage' => $f])}}" class="btn btn-danger"  onclick="return confirm({{ __("Do you really want to delete the question?") }})">{{ __("Delete") }}</a>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                        <br><br>
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link" data-toggle="collapse" data-target="#addFrage">
                                                    {{ __("Add Question") }}
                                                </button>
                                            </h5>
                                        </div>

                                        <div id="addFrage" class="collapse" data-parent="#accordion">
                                            <div class="card-body">
                                                <form method="POST" action="{{ route('admin.frage.add', ['document' => $document]) }}">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="title_de">{{ __("Title_de") }}</label>
                                                        <input type="text" class="form-control" id="title_de" name="title_de" required>
                                                        <small class="form-text text-muted">{{ __("Explanation Question Title DE") }}</small>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="beschreibung_de">{{ __("Description_de") }}</label>
                                                        <textarea class="form-control tinymce" id="beschreibung_de" rows="3" name="description_de"></textarea>
                                                        <small class="form-text text-muted">{{ __("Explanation Question Description DE") }}</small>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="title_en">{{ __("Title_en") }}</label>
                                                        <input type="text" class="form-control" id="title_en" name="title_en" required>
                                                        <small class="form-text text-muted">{{ __("Explanation Question Title EN") }}</small>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="beschreibung_en">{{ __("Description_en") }}</label>
                                                        <textarea class="form-control tinymce" id="beschreibung_en" rows="3" name="description_en"></textarea>
                                                        <small class="form-text text-muted">{{ __("Explanation Question Description EN") }}</small>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="frageType">{{ __("Type of Question") }}</label>
                                                        <select class="form-control" id="frageType" name="type">
                                                            <option value="{{\App\Models\Frage::TEXT}}">{{ __("Text") }}</option>
                                                            <option value="{{\App\Models\Frage::TITEL}}">
                                                                {{ __("Information/Headline") }}</option>
                                                        </select>
                                                        <small class="form-text text-muted">
                                                            {{ __("Explanation Question Text") }}
                                                            <br>
                                                            {{ __("Explanation Question Information/Headline") }}
                                                        </small>
                                                    </div>
                                                    <br>
                                                    <button type="submit" class="btn btn-primary">{{ __("Save") }}</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ul>
                        </div>
                    </div>
                <br>


                <div class="card">
                    <div class="card-header">{{ __("Checklist") }}</div>
                    <div class="card-body">
                        <ul class="list-group">
                            <div id="accordionCheck">

                                @foreach($document->checklisten as $index => $c)

                                    <div class="card">

                                        <div class="card-header" id="check{{$index}}Collapse">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link" data-toggle="collapse" data-target="#check{{$index}}">
                                                    {{$c->get_text()}}
                                                </button>
                                            </h5>
                                            <div id="check{{$index}}" class="collapse" data-parent="#accordion">
                                                <div class="card-body">
                                                    <form method="POST" action="{{ route('admin.check.edit', ['document' => $document, 'check_item' => $c]) }}">
                                                        @csrf
                                                        <div class="form-group">
                                                            <label for="title_de">{{ __("Title_de") }}</label>
                                                            <input type="text" class="form-control" id="title_de" name="title_de" required value="{{$c->text_de}}">
                                                            <small class="form-text text-muted">{{ __("Explanation Checklist Item Title DE") }}</small>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="title_en">{{ __("Title_en") }}</label>
                                                            <input type="text" class="form-control" id="title_en" name="title_en" required value="{{$c->text_en}}">
                                                            <small class="form-text text-muted">{{ __("Explanation Checklist Item Title EN") }}</small>
                                                        </div>
                                                        <br>
                                                        <button type="submit" class="btn btn-primary">{{ __("Save") }}</button>
                                                        <a href="{{route('admin.check.delete', ['checkitem' => $c])}}" class="btn btn-danger"  onclick="return confirm({{ __("Do you really want to delete this checklist item?") }})">{{ __("Delete") }}</a>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                    <br><br>
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#addCheck">
                                                {{ __("Add Checklist Item") }}
                                            </button>
                                        </h5>
                                    </div>

                                    <div id="addCheck" class="collapse" data-parent="#accordionCheck">
                                        <div class="card-body">
                                            <form method="POST" action="{{ route('admin.check.add', ['document' => $document]) }}">
                                                @csrf
                                                <div class="form-group">
                                                    <label for="title_de">{{ __("Title_de") }}</label>
                                                    <input type="text" class="form-control" id="title_de" name="title_de" required>
                                                    <small class="form-text text-muted">{{ __("Explanation Checklist Item Title DE") }}</small>
                                                </div>
                                                <div class="form-group">
                                                    <label for="title_en">{{ __("Title_en") }}</label>
                                                    <input type="text" class="form-control" id="title_en" name="title_en" required>
                                                    <small class="form-text text-muted">{{ __("Explanation Checklist Item Title EN") }}</small>
                                                </div>
                                                <br>
                                                <button type="submit" class="btn btn-primary">{{ __("Save") }}</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <a href="{{route('admin.documents.list')}}" class="btn btn-danger">{{ __("Back") }}</a>
                            </div>
                        </ul>
                    </div>
                </div>


                </div>
            </div>
        </div>
    </div>
@endsection
