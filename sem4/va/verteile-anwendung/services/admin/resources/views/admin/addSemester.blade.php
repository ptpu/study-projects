@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __("Create Semester") }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.semester.add') }}">
                            @csrf
                            <div class="form-group">
                                <label for="title">{{ __("Title") }}</label>
                                <input type="type" class="form-control" id="title" name="title" required>
                                <small class="form-text text-muted">{{ __("Explanation Semester Title") }}</small>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary">{{ __("Create") }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
