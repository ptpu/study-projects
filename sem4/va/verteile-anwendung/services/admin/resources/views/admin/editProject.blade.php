<?php /** @var \App\Models\Project $p */ ?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __("Edit Project") }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('admin.project.edit', ['project' => $p]) }}">
                            @csrf
                            <div class="form-group">
                                <label for="title">{{ __("Title") }}</label>
                                <input type="text" class="form-control" id="title" name="title" value="{{$p->title}}" required>
                                <small class="form-text text-muted">{{ __("Explanation Project Title") }}</small>
                            </div>
                            <div class="form-group">
                                <label for="beschreibung">{{ __("Description") }}</label>
                                <textarea class="form-control" id="beschreibung" rows="3" name="description">{{$p->description}}</textarea>
                                <small class="form-text text-muted">{{ __("Explanation Project Description") }}</small>
                            </div>
                            <div class="form-group">
                                <label for="semester">{{ __("Semester") }}</label>
                                <select class="form-control" id="semester" name="semesterId">
                                    @foreach($semesters as $s)
                                        <option value="{{ $s->id }}" {{ ($s->id == $p->semester_id) ? 'selected' : '' }}>{{ $s->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <br>
                            <a href="{{route('admin.projects.edit')}}" class="btn btn-danger">{{ __("Back") }}]</a>
                            <button type="submit" class="btn btn-primary">{{ __("Save") }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
