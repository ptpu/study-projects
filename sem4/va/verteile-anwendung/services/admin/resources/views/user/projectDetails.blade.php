@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('home.projects.list')}}">{{ __("Home") }}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{$project->title}}</li>
                        </ol>
                    </nav>
                    <div class="card-header">
                        <a href="{{route('export', ['project' => $project])}}"  class="btn btn-secondary" style=".btn-secondary {color:#FFF; background-color:#2fa360;}" title="{{ __("Export") }}"><i class="fas fa-file-export"></i>
                            {{ __("Export") }}</a>
                    </div>
                        <div class="card-body">

                            @foreach($documents as $d)
                                <a href="{{route('home.document.fill', ['project' => $project, 'document' => $d])}}">

                                    <div class="card">
                                        <div class="card-header">
                                            @php
                                                $progress = $d->getProgress($project);
                                                //$owner = $d->getOwner($project);

                                            @endphp
                                            <div class="w-100">
                                                <h6 class="w-100 float-left ">{{$d->getOwner($project)}}</h6>
                                                <h5 class="float-left">{{$d->get_title()}}</h5>
                                                <h6 class="float-right">{{$progress}}%</h6>
                                            </div>
                                            <div class="clearfix"></div>


                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: {{$progress}}%" aria-valuenow="{{ $progress }}" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                        </div>
                                    </div>

                                </a>

                            @endforeach





                        </div>

                </div>

            </div>
        </div>
    </div>
@endsection
