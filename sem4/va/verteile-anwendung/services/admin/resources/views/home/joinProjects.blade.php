@extends('layouts.app')

@php($user = auth()->user())

@push('scripts')
    <script type="text/javascript">
        function ProjektLoeschen(form, title) {
            if ( confirm("{{ __("Do you really want to delete the project?") }}") ) {
                form.submit();
            }
        }
        function semesterLoeschen(form, title) {
            if ( confirm("{{ __("Do you really want to delete the semester?") }}") ) {
                form.submit();
            }
        }
    </script>
@endpush
@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    @if($user->is_admin)
                        <div class="card-header">{{ __("Edit Project") }}</div>
                    @else
                        <div class="card-header">{{ __("Join Project") }}</div>
                    @endif

                    <br>
                    <div id="accordion">
                        @foreach($semester as $index => $s)
                            <div class="container">
                            <div class="card">
                                <a href="#" data-toggle="collapse" data-target="#collapse{{$index}}" aria-controls="collapse{{$index}}">
                                    <div class="card-header">
                                        <h5 class="mb-0 pb-3">
                                            {{ $s->title }}
                                            @if($user->is_admin)
                                                <form action="{{ route('admin.semester.delete', ['semester' => $s]) }}" method="post" class="float-right">
                                                    <input class="btn btn-danger" type="button" onclick="semesterLoeschen(this.form, '{{$s->title}}')" value="{{ __("Delete") }}" />
                                                    {!! method_field('delete') !!}
                                                    {!! csrf_field() !!}
                                                </form>
                                            @endif
                                        </h5>
                                    </div>
                                </a>


                                <div id="collapse{{$index}}" class="collapse" data-parent="#accordion">
                                    <div class="card-body">
                                        @foreach($s['projects'] as $index => $p)
                                            @php($meins = $user->projects->contains($p->id))
                                            <div class="container">
                                                <div class="card @if($meins) text-white bg-secondary @endif">
                                                    <div class="card-body">
                                                        <h5 class="card-title">{{ $p->title }}</h5>
                                                        <p class="card-text">{{$p->description}}</p>
                                                        @if(!$user->is_admin)
                                                            <a href="{{route('home.project.join', ['project' => $p])}}" class="btn btn-primary @if($meins) disabled @endif">{{ __("Join") }}</a>
                                                        @else
                                                            <span class="float-right">
                                                                <a href="{{route('export', ['project' => $p])}}" class="btn btn-secondary" title="Export"><i class="fas fa-file-export"></i></a>
                                                                <a href="{{route('admin.project.edit', ['project' => $p])}}" class="btn btn-primary">{{ __("Edit") }}</a>
                                                                <form action="{{ route('admin.project.delete', ['project' => $p]) }}" method="post" class="float-right">
                                                                    <input class="btn btn-danger" type="button" onclick="ProjektLoeschen(this.form, '{{$p->title}}')" value="{{ __("Delete") }}" />
                                                                    {!! method_field('delete') !!}
                                                                    {!! csrf_field() !!}
                                                                </form>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            </div>

                        @endforeach


                    </div>
                    <br>


                </div>
            </div>
        </div>
    </div>
@endsection
