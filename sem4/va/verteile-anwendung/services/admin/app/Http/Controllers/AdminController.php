<?php

namespace App\Http\Controllers;

use App\Models\ChecklisteItem;
use App\Models\Document;
use App\Models\Frage;
use App\Models\Project;
use App\Models\Semester;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function get_addSemester()
    {
        return view("admin.addSemester");
    }

    public function createSemester(Request $request)
    {
        try {
            $this->validate($request, [
                "title" => "required"
            ]);
        } catch (ValidationException $e) {
            return redirect()->back()->with('error', trans('semester.create.error', [ ], $request->getLocale()));
        }

        $title = $request->input('title');

        $semester = new Semester();
        $semester->title = $title;
        $semester->save();

        return redirect()->route('admin.semester.add')
            ->with('success', trans('semester.create.success', [ 'title' => $title ], $request->getLocale()));
    }

    public function deleteSemester(Request $request, Semester $semester)
    {
        foreach($semester->projects() as $project){
            $project->delete();
        }

        try {
            $semester->delete();
        } catch (\Exception $e) {
            return redirect()->back()->with('error', trans('semester.delete.error', [ ], $request->getLocale()));
        }

        return redirect()->back()->with('success', trans('semester.delete.success', [ 'title' => $semester->title ], $request->getLocale()));
    }

    public function get_addProject()
    {
        $semesters = Semester::all();
        return view("admin.addProject", [
            'semesters' => $semesters
        ]);
    }

    public function createProject(Request $request)
    {
        try {
            $this->validate($request, [
                "title" => "required",
                "semesterId" => "required"
            ]);
        } catch (ValidationException $e) {
            return redirect()->back()->with('error', trans('project.create.error', [ ], $request->getLocale()));
        }

        $projectTitle = $request->input('title');
        $semesterId = $request->input('semesterId');
        $description = $request->input("description", "");
        $semester = Semester::findOrFail($semesterId);

        $project = new Project();
        $project->title = $projectTitle;
        $project->description = $description;
        $project->semester()->associate($semester);
        $project->save();

        return redirect()->route('admin.project.add')
            ->with('success', trans('project.create.success', [ 'title' => $projectTitle ], $request->getLocale()));
    }

    public function deleteProject(Request $request, Project $project)
    {
        try {
            $project->delete();
        } catch (\Exception $e) {
            return redirect()->back()->with('error', trans('project.create.error', [], $request->getLocale()));
        }

        return redirect()->back()->with('success', trans('project.delete.success', [ 'title' => $project->title ], $request->getLocale()));
    }

    public function viewEditProject(Project $project)
    {
        $semester = Semester::all();
        return view('admin.editProject', [
            'p' => $project,
            'semesters' => $semester
        ]);
    }

    public function editProject(Request $request, Project $project)
    {
        try {
            $this->validate($request, [
                "title" => "required",
                "semesterId" => "required"
            ]);
        } catch (ValidationException $e) {
            return redirect()->back()->with('error', trans('project.save.error', [], $request->getLocale()));
        }

        $projectTitle = $request->input('title');
        $semesterId = $request->input('semesterId');
        $description = $request->input("description", "");
        $semester = Semester::findOrFail($semesterId);

        $project->title = $projectTitle;
        $project->semester_id = $semesterId;
        $project->description = $description;
        $project->semester()->associate($semester);
        $project->save();

        return redirect()->back()->with('success', trans('project.save.success', [ 'title' => $projectTitle ], $request->getLocale()));
    }

    public function get_addDocument()
    {
        return view('admin.addDocument');
    }

    public function addDocument(Request $request)
    {
        try {
            $this->validate($request, [
                'title_de' => 'required',
                'title_en' => 'required'
            ]);
        } catch (ValidationException $e) {
            return redirect()->back()->with('error', trans('document.create.error', [], $request->getLocale()));
        }

        $documentTitle_en = $request->input('title_en');
        $description_en = $request->input('description_en');
        $documentTitle_de = $request->input('title_de');
        $description_de = $request->input('description_de');
        $excludeFromExport = $request->input('exclude_from_export');

        if(!$excludeFromExport){
            $excludeFromExport = false;
        } else {
            $excludeFromExport = true;
        }

        $document = new Document();
        $document->title_de = $documentTitle_de;
        $document->title_en = $documentTitle_en;
        $document->description_de = $description_de;
        $document->description_en = $description_en;
        $document->exclude_from_export = $excludeFromExport;
        $document->save();

        return redirect()->back()->with('success', trans('document.create.success', [ 'title' => $document->get_title() ], $request->getLocale()));
    }

    public function listDocuments()
    {
        $documents = Document::all();
        return view('admin.listDocuments', [
            'documents' => $documents
        ]);
    }

    public function details_documents(Request $request, Document $document)
    {
        $fragen = $document->fragen;
        $checklisten = $document->checklisten();

        return view('admin.editDocument', [
            'fragen' => $fragen,
            'document' => $document,
            'checklisten' => $checklisten
        ]);
    }

    public function editDocumentTitle(Request $request, Document $document)
    {
        $document->title_en = $request->input('title_en');
        $document->title_de = $request->input('title_de');
        $excludeFromExport = $request->input('exclude_from_export');

        if(!$excludeFromExport){
            $excludeFromExport = false;
        } else {
            $excludeFromExport = true;
        }

        $document->exclude_from_export = $excludeFromExport;
        $document->save();

        return redirect()->back()->with('success', trans('document.save.success', [ 'title' => $document->get_title() ], $request->getLocale()));
    }

    public function deleteDocument(Request $request, Document $document)
    {
        try {
            $document->delete();
        } catch (\Exception $e) {
            return redirect()->back()->with('error', trans('document.delete.error', [], $request->getLocale()));
        }
        return(redirect(route('admin.documents.list')));
    }

    public function updateTasksOrder(Request $request)
    {

        $this->validate($request, [
            'tasks.*.order' => 'required|numeric',
        ]);

        $tasks = Document::all();

        foreach ($tasks as $task) {
            $id = $task->id;
            foreach ($request->tasks as $tasksNew) {
                if ($tasksNew['id'] == $id) {
                    $task->update(['placement' => $tasksNew['order']]);
                }
            }
        }

        return response('Updated Successfully.', 200);
    }
    public function addFrage(Request $request, Document $document)
    {
        try {
            $this->validate($request, [
                'title_de' => 'required',
                'title_en' => 'required',
                'type' => 'required'
            ]);
        } catch (ValidationException $e) {
            return redirect()->back()->with('error', trans('question.add.error', [], $request->getLocale()));
        }

        $type = $request->input('type');
        $title_de = $request->input('title_de');
        $description_de = $request->input('description_de');
        $title_en = $request->input('title_en');
        $description_en = $request->input('description_en');
        $questionCount = $document->fragen()->count();

        $frage = new Frage();
        $frage->title_de = $title_de;
        $frage->title_en = $title_en;
        $frage->type = $type;
        $frage->description_de = $description_de;
        $frage->description_en = $description_en;
        $frage->document_id = $document->id;
        $frage->level = 0;

        $frage->placement = $questionCount;
        $frage->save();

        return redirect()->back()
            ->with('success', trans('question.add.success', [ 'title' => $frage->get_title() ], $request->getLocale()));

    }

    public function editFrage(Request $request, Document $document, Frage $frage)
    {
        try {
            $this->validate($request, [
                'title_de' => 'required',
                'title_en' => 'required',
                'type' => 'required'
            ]);
        } catch (ValidationException $e) {
            return redirect()->back()->with('error', trans('question.edit.error', [], $request->getLocale()));
        }

        $type = $request->input('type');
        $title_de = $request->input('title_de');
        $description_de = $request->input('description_de');
        $title_en = $request->input('title_en');
        $description_en = $request->input('description_en');

        $frage->title_en = $title_en;
        $frage->title_de = $title_de;
        $frage->type = $type;
        $frage->description_en = $description_en;
        $frage->description_de = $description_de;
        $frage->document_id = $document->id;
        $frage->save();

        return redirect()->back()
            ->with('success', trans('question.edit.success', [ 'title' => $frage->get_title()], $request->getLocale()));
    }

    public function levelFrage(Request $request, Frage $frage)
    {
        $direction = $request->get('direction');

        if(!in_array($direction, ['left', 'right'])){
            return redirect()->back()->with('error', trans('question.level.error', [], $request->getLocale()));
        }

        $currLevel = $frage->level;

        if($direction == 'left'){
            if($currLevel > 0){
                $frage->level = $currLevel - 1;
                $frage->save();
            }
        } else if($direction == 'right'){
            if($currLevel < 5){
                $frage->level = $currLevel + 1;
                $frage->save();
            }
        }

        return redirect()->back();
    }

    public function deleteFrage(Request $request, Frage $frage)
    {
        $document_id = $frage->document_id;

        $questionsToMove = Frage::where([
            ['document_id', '=', $document_id],
            ['placement', '>', $frage->placement]
        ]);
        foreach($questionsToMove as $f){
            $f->placement = $f->placement - 1;
            $f->save();
        }

        try {
            $frage->delete();
        } catch (\Exception $e) {
            return redirect()->back()->with('error', trans('question.delete.error', [], $request->getLocale()));
        }
        return redirect()->back()->with('success', trans('question.delete.success', [], $request->getLocale()));
    }

    public function addCheck(Request $request, Document $document)
    {
        try {
            $this->validate($request, [
                'title_de' => 'required',
                'title_en' => 'required'
            ]);
        } catch (ValidationException $e) {
            return redirect()->back()
                ->with('error', trans('checklist.item.add.error', [], $request->getLocale()));
        }

        $count = $document->checklisten()->count();
        $title_en = $request->input('title_en');
        $title_de = $request->input('title_de');

        $checklistItem = new ChecklisteItem();
        $checklistItem->text_en = $title_en;
        $checklistItem->text_de = $title_de;
        $checklistItem->placement = $count;
        $checklistItem->document_id = $document->id;
        $checklistItem->save();

        return redirect()->back()
            ->with('success', trans('checklist.item.add.success', [ 'title' => $checklistItem->get_text() ], $request->getLocale()));

    }

    public function editCheck(Request $request, Document $document, ChecklisteItem $check_item)
    {

        try {
            $this->validate($request, [
                'title_de' => 'required',
                'title_en' => 'required'
            ]);
        } catch (ValidationException $e) {
            return redirect()->back()
                ->with('error', trans('checklist.item.edit.error', [], $request->getLocale()));
        }

        $title_en = $request->input('title_en');
        $title_de = $request->input('title_de');

        $check_item->text_en = $title_en;
        $check_item->text_de = $title_de;
        $check_item->document_id = $document->id;

        $check_item->save();

        return redirect()->back()
            ->with('success', trans('checklist.item.edit.success', [ 'title' => $check_item->get_text() ], $request->getLocale()));

    }

    public function deleteCheck(Request $request, ChecklisteItem $checklisteItem)
    {
        try {
            $checklisteItem->delete();
        } catch (\Exception $e) {
            return redirect()->back()
                ->with('error', trans('checklist.item.delete.error', [], $request->getLocale()));
        }
        return redirect()->back()
            ->with('success', trans('checklist.item.delete.success', [], $request->getLocale()));
    }
}
