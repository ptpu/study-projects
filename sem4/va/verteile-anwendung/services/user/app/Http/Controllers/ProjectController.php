<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\Project;
use App\User;
use App\Verantwortlicher;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function attachUserToProject(Request $request, Project $project)
    {
        /** @var User $user */
        $user = auth()->user();

        if( $user->projects->contains($project->id) ){
            return redirect()->back()
                ->with('warning', trans('project.join.warning', [], $request->getLocale()));
        }

        $user->projects()->save($project);
        $projectTitle = $project->title;

        return redirect()->route('home.projects.list')
            ->with('success', trans('project.join.success', [ 'title' => $projectTitle ], $request->getLocale()));
    }

    public function leaveProject(Request $request, Project $project)
    {
        /** @var User $user */
        $user = auth()->user();
        $project->users()->detach($user->id);
        $projectTitle = $project->title;

        return redirect()->route('home.projects.list')
            ->with('success', trans('project.leave.success', [ 'title' => $projectTitle ], $request->getLocale()));
    }

    public function details(Project $project)
    {
        $documents = Document::all();

        return view('user.projectDetails', [
            'project' => $project,
            'documents' => $documents
        ]);
    }
}
