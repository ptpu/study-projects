<?php

namespace App\Http\Controllers;

use App\Models\Semester;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }



    public function listProjects(){
        /** @var User $user */
        $user = auth()->user();
        $myProjects = $user->projects;
        return view('home.projectsList', [
            'myProjects' => $myProjects
        ]);

    }

    public function listJoinProjects(){

        $semester = Semester::all();
        /** @var Semester $s */
        foreach( $semester as $s ){
            $s['projects'] = $s->projects;
        }
        return view('home.joinProjects', ['semester' => $semester]);
    }
}
