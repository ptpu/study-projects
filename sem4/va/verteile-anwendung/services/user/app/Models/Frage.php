<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

/**
 * @property string title_de
 * @property string description_de
 * @property string title_en
 * @property string description_en
 * @property int id
 * @property int type
 * @property int document_id
 * @property int|mixed level
 * @property int placement
 */
class Frage extends Model
{
    const TEXT = 1;
    const CHECKBOX = 2;
    const TITEL = 3;

    public function get_title(){
        $lang=App::getLocale();
        switch ($lang){
            case "en":
                return $this->title_en;
            case "de":
                return $this->title_de;
        }
        return "Meeeep";
    }
    public function get_description()
    {
        $lang = App::getLocale();
        switch ($lang) {
            case "en":
                return $this->description_en;
            case "de":
                return $this->description_de;
        }
        return "Meeeep";
    }
    public function document(){
        return $this->belongsTo(Document::class);
    }

    public function antworten(){
        return $this->hasMany(Antwort::class);
    }

    public function getAntwort(Project $project) {
        $a = Antwort::where([
            ['frage_id', '=', $this->id],
            ['project_id', '=', $project->id]
        ])->first();
        return ($a)?$a->antwort:"";
    }

    public function renderFormElement(Project $project = null){
        $id = $this->id;
        $title=$this->get_title();
        $desc=$this->get_description();
        $value = "";
        if($project){
            $antwort = Antwort::where([
                ['frage_id', '=', $id],
                ['project_id', '=', $project->id]
            ])->first();

            if($antwort && $antwort->antwort){
                $value = $antwort->antwort;
            }
        }
        switch($this->type){
            case self::TEXT:
                return "
                <div class='form-group'>
                    <i class='form-text text-muted'>$desc</i> <br>
                    <label for='frage$id'>$this->title</label>
                    <textarea class='form-control tinymce' id='frage$id' rows='3' name='frage[$id]' onclick='myfunction()'>$value</textarea>
                </div>
                ";
            case self::CHECKBOX:
                break;
            case self::TITEL:
                return "
                <div class='form-group'>
                    <i class='form-text text-muted'>$desc</i> <br>
                </div>
                ";
                break;
            default:
                return "<h1>ERROR! TYPE UNDEFINED!</h1>";
                break;
        }
    }
}
