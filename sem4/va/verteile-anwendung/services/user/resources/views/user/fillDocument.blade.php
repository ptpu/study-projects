@extends('layouts.app')
@php
/** @var \App\Models\Document $document */
@endphp
@section('content')
    <script type="application/javascript">
        let wantToSave = false;
        let unsaved = false;

    </script>
    <form action="{{route('home.document.save', ['document' => $document, 'project' => $project])}}" method="post">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card" >
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{route('home.projects.list')}}">{{ __("Home") }}</a></li>
                            <li class="breadcrumb-item"><a href="{{route('home.project.details', ['project' => $project])}}">{{ $project->title }}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{$document->get_title()}}</li>
                        </ol>
                    </nav>

                    <div class="form-group  pl-4 pr-4">
                        <label for="owner" >{{ __("Responsible") }}</label>
                        <div class="pl-3"></div>
                        <input type="type" class="form-control d-flex" placeholder="{{ __("Please enter the person responsible") }}" id="owner" value="{{$owner}}" name="owner" >
                    </div>
                    <div class="card-body">
                        <div id="accordion">


                            @csrf
                                @foreach($document->fragen as $f)
                                    @php
                                        /** @var \App\Models\Frage $f */
                                    @endphp

                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#f_{{$f->id}}">
                                                    {{$f->get_title()}}
                                                </button>
                                            </h5>
                                        </div>

                                        <div id="f_{{$f->id}}" class="collapse" data-parent="#accordion">
                                            <div class="card-body">
                                                {!! $f->renderFormElement($project) !!}
                                            </div>
                                        </div>
                                    </div>

                                @endforeach


                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-4 ">
                <div class="card mb-5 sticky-top">
                    <div class="card-body text-center">

                        <div class="btn-group text-center w-100" role="group" aria-label="Basic example">
                            <a href="{{route('home.project.details', ['project' => $project])}}" class="btn btn-danger btn-lg">{{ __("Back") }}</a>
                            <button type="submit" onclick="wantToSave = true;" class="btn btn-primary btn-lg">
                                {{ __("Save") }}</button>
                        </div>
                    </div>
                </div>
                <div class="card" >
                    <div class="card-body">
                        <div id="accordion_2">
                            @foreach($document->checklisten as $c)
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="mb-0">
                                            <label class="pr-4" for="check_{{$c->id}}">{{$c->get_text()}}</label>
                                            <span class="float-right">
                                                <input type="checkbox" class="form-check-input" name="check[{{$c->id}}]" id="check_{{$c->id}}" @if($c->getChecked($project))checked="checked"@endif>
                                            </span>
                                        </h5>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    </form>
    <script type="application/javascript">
        $(document).ready(function () {

            let unsaved;
            $(window).bind('beforeunload', function() {
                if (unsaved && wantToSave === false) {
                    return confirm("You have unsaved changes on this page. Do you want to leave this page and discard your changes or stay on this page?");
                }
            });


            $(".collapse").on('shown.bs.collapse', function(event){
                event.target.addEventListener('DOMCharacterDataModified', myfunction());
            });

            function myfunction(){
                unsaved = true;
                console.log("vun true");
            }
            // Monitor dynamic inputs
            $(document).on('change', ':input', function(){ //triggers change in all input fields including text type
                unsaved = true;
                console.log("xun true");
            });

        });



        //if ($('#questionarea').serialize() != $('#questionarea').data('serialize') && wantToSave === false)

    </script>


@endsection
