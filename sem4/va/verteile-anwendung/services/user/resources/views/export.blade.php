<style>

</style>
<?php
    /* @var $project \App\Models\Project */
    /* @var $documents \App\Models\Document[] */
    /* @var $frage \App\Models\Frage */
    /* @var $antwort string */

?>
<h1>{{ __("Project: ") }} {{$project->title}}</h1>

@foreach($documents as $document)
    @if($document->exclude_from_export)
        @continue
    @endif
    <div class="pagebreak"></div>
    <h2>{{$document->get_title()}}</h2>
    <span>{{$document->get_description()}}</span>
    <hr>
    @foreach($document->fragen as $frage)

        @php
            $antwort = $frage->getAntwort($project);
            $level = 3 + $frage->level;
        @endphp

        <h{{$level}}>{{$frage->get_title()}}</h{{$level}}>
        <small><i>{!! $frage->get_description() !!}</i></small>

        {!! $antwort !!}
        <br>
    @endforeach
@endforeach






