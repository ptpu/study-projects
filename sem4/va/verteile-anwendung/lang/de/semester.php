<?php

return [
    "create" => [
        "success" => "Semester \":title\" erfolgreich angelegt.",
        "error" => "Es ist ein Fehler aufgetreten"
    ],
    "delete" => [
        "success" => "Semester \":title\" erfolgreich gelöscht!",
        "error" => "Es ist ein Fehler aufgetreten"
    ]
];
