<?php

return [
    "item" => [
        "add" => [
            "success" => "Checkliste Item \":title\" erfolgreich angelegt.",
            "error" => "Ein Fehler ist aufgetreten."
        ],
        "edit" => [
            "success" => "Checkliste Item \":title\" erfolgreich bearbeitet.",
            "error" => "Ein Fehler ist aufgetreten."
        ],
        "delete" => [
            "success" => "Checkliste Item erfolgreich gelöscht.",
            "error" => "Ein Fehler ist aufgetreten."
        ]
    ]
];
