<?php

return [
    "create" => [
        "success" => "Project \":title\" successfully created.",
        "error" => "An error occurred."
    ],
    "delete" => [
        "success" => "Project \":title\" successfully deleted!",
        "error" => "An error occurred."
    ],
    "save" => [
        "success" => "Project \":title\" successfully saved.",
        "error" => "An error occurred."
    ],
    "join" => [
        "warning" => "You are already in this project",
        "success" => "Project \":title\" successfully joined."
    ],
    "leave" => [
        "success" => "Project \":title\" successfully left."
    ]
];
