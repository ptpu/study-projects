<?php

return [
    "create" => [
        "success" => "Document \":title\" successfully created!",
        "error" => "An error occurred."
    ],
    "save" => [
        "success" => "Document \":title\" successfully edited."
    ],
    "delete" => [
        "error" => "An error occurred."
    ]
];
