const DB_HOST = process.env.DB_HOST || "localhost"
const DB_USER = process.env.DB_USER || "mrbs"
const DB_PASSWORD = process.env.DB_PASSWORD || "mrbs"
const DB_DB = process.env.DB_DB || "mrbs"
const APP_PORT = process.env.APP_PORT || "3000"

const mariadb = require('mariadb');
const express = require('express')

let error = {
  "code": 0,
  "message": "string"
}

function roomInfoFromData(data) {
  if (data.length === 1) {
    return {
      "roomNumber": data[0].room_name,
      "roomType": data[0].type,
      "roomOwner": data[0].room_admin_email,
      "roomOwnerSubtile": "?"
    };
  }
}

function roomStatusFromData(data) {
  let status = "free";
  let current = {};
  let upcoming = []

  for (let i = 0; i < data.length; i++) {
    let elem = data[i];
    let start = new Date(elem.start_time * 1000);
    let end = new Date(elem.end_time * 1000);

    let event = {
      "eventName": elem.name,
      "timeFrom": start.toISOString(),
      "timeTo": end.toISOString()
    }

    if (Date.now() > start && Date.now() < end) {
      current = event;
      status = "booked";
    } else if (Date.now() < start) {
      upcoming.push(event);
    }
  }

  return {
    "currentEvent": current,
    "upcomingEvents": upcoming,
    "roomStatusInfo": status
  }
}


mariadb.createConnection({
  host: DB_HOST,
  user: DB_USER,
  password: DB_PASSWORD,
  database: DB_DB
}).then((conn) => {
  const app = express();

  app.get("/roomInfoByNumber/:roomNumber", async (req, res) => {
    let roomNo = req.params.roomNumber;
    const data = await conn.query("SELECT `room_name`, `type`, `room_admin_email` FROM `mrbs_room` WHERE `room_name` = ?", roomNo);
    res.send(roomInfoFromData(data));
  });

  app.get("/roomInfoById/:id", async (req, res) => {
    let id = req.params.id;
    const data = await conn.query("SELECT `room_name`, `type`, `room_admin_email` FROM `mrbs_room` WHERE `id` = ?", id);
    res.send(roomInfoFromData(data));
  });

  app.get("/roomStatusByNumber/:roomNumber", async (req, res) => {
    let roomNo = req.params.roomNumber;
    const data = await conn.query("SELECT e.`start_time`, e.`end_time`, e.`name` FROM `mrbs_entry` AS e JOIN `mrbs_room` AS a ON e.room_id = a.id WHERE a.room_name = ?", roomNo);
    res.send(roomStatusFromData(data));
  });

  app.get("/roomStatusById/:id", async (req, res) => {
    let id = req.params.id;
    const data = await conn.query("SELECT `start_time`, `end_time`, `name` FROM `mrbs_entry` WHERE room_id = ?", id);
    res.send(roomStatusFromData(data));
  });

  app.listen(parseInt(APP_PORT), () => {
    console.log(`Server running on port ${APP_PORT}`);
  });
}).catch((err) => {
  console.error("Database connection failed");
  console.error(err);
  process.exit(1);
});
