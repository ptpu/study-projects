package blatt01.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Collections;
import java.util.Scanner;

public class jdbc {

    static Connection con;

	final static String SQLdriver = "org.postgresql.Driver";
	final static String JDBC_URL = "jdbc:postgresql://localhost:5432/postgres"; 
	final static String user = "postgres";
	final static String password = "dbs";

    public static void main(String[] args) throws Exception {
        Class.forName(SQLdriver); 
		con = DriverManager.getConnection(JDBC_URL, user, password);

		printBuchtitelBuchAusgeliehen();
		//insertBuchAusleihen();
    }

    private static void printBuchtitelBuchAusgeliehen() throws Exception {
		
		// Eingabe Leser-Id
		Scanner in = new Scanner(System.in);
		int leserId;
		try {
			System.out.print("Leser-Id eingeben: ");
			leserId = Integer.valueOf(in.nextLine());
		} finally {
			in.close();
		}
		
		// Prepared Statement
		PreparedStatement ps = con.prepareStatement(
			"SELECT B.titel FROM ausleihe.buchex AS B " +
			"JOIN ausleihe.ausleihe AS A ON B.isbn=A.isbn AND B.explnr=A.explnr " +
			"WHERE A.leserid = ?");
		ps.setInt(1, leserId);
		ResultSet rs = ps.executeQuery();

		 // Ausgabe
		System.out.println("Buchtitel\t");
		System.out.println(String.join("", Collections.nCopies(20, "=")));
		while (rs.next()) {
			System.out.printf("%s\n", rs.getString("Titel"));
		}
		rs.close();	
    }

	public static void insertBuchAusleihen() throws Exception {
		Scanner in = new Scanner(System.in);

		int leserId;
		String isbn;

		try {
			//Eingabe Leser-Id
			System.out.print("Leser-Id eingeben: ");
			leserId = Integer.valueOf(in.nextLine());
			
			//Eingabe ISBN
			System.out.print("ISBN eingeben: ");
			isbn = in.nextLine();
		} finally {
			in.close();
		}

		// Abfrage ob noch Exemplar vorhanden ist
		PreparedStatement ps = con.prepareStatement(
			"SELECT B.isbn, B.explnr FROM ausleihe.buchex AS B " +
			"WHERE B.isbn = ? " +
			"EXCEPT " +
			"SELECT A.isbn, A.explnr FROM ausleihe.ausleihe AS A " +
			"ORDER BY explnr ASC " + 
			"LIMIT 1");
		ps.setString(1, isbn);
		ResultSet rs = ps.executeQuery();

		// Wenn rs nicht leer ist
		if(rs != null && rs.next()){
			ps = con.prepareStatement(
			"INSERT INTO ausleihe.ausleihe (isbn, explnr, datum, leserid) " +
			"VALUES (?, ?, CURRENT_DATE, ?)");

			ps.setString(1, isbn);
			ps.setInt(2, rs.getInt("explnr"));
			ps.setInt(3, leserId);

			ps.executeUpdate();

			System.out.println("Erledigt!");
		}else{
			throw new Exception("Alle Bücher mit dieser ISBN sind bereits ausgeliehen. Sorry");
		}
		rs.close();
	}
}