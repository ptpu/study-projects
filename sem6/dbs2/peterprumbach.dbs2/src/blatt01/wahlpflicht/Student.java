package blatt01.wahlpflicht;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Student {
    @Id
    int MatNr;
    String Name;

    @ManyToMany(mappedBy="studenten")
    List<Modul> modul;

    public Student() {

    }

    public Student(int MatNr, String Name) {
        super();
        this.MatNr=MatNr;
        this.Name=Name;
        this.modul = new ArrayList<Modul>();
    }
}