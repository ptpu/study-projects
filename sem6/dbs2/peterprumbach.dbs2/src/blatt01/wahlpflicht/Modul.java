package blatt01.wahlpflicht;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OrderColumn;

@Entity
public class Modul {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="vid")
    int Id;

    int Semester;
    String Titel;
    
    @ElementCollection
    @CollectionTable(
        name="Modul_Studiengang",
        joinColumns=@JoinColumn(name="modul_vid")
    )
    List<String> Studiengang = new ArrayList<String>();

    @ManyToMany
    @OrderColumn(name="position")
    List<Student> studenten;

    public Modul() {

    }

    public Modul(int Semester, String Titel, String... Studiengang) {
        super();
        this.Semester=Semester;
        this.Titel=Titel;
        for(String s: Studiengang){
            this.Studiengang.add(s);
        }
        this.studenten = new ArrayList<Student>();
    }

    public Modul einschreiben(Student s) {
        this.studenten.add(s);
        return this;
    }
}
