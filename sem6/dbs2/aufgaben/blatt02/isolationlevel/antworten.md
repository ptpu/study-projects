# Isolation Level: Aufgabe

a) Gehalt von P1 kann 600 oder 150 betragen. Es ist zufällig, welche Transaktion ausgeführt wird. Eine Transaktion kann die Änderungen commiten (bspw. T1), die andere (bspw. T2) erhält eine Fehlermeldung nach welcher dann auch dessen Änderungen zurückgerollt werden.

b) Gehalt von P1 kann 600 oder 150 betragen. Updates können so also verloren gehen.

c) Gehalt von P1 kann entweder 650 (T1 vor T2) oder 900 betragen (T2 vor T1). Dies ist möglich, da bei Read Uncommited ein sogenannter Dirty Read auftreten kann, bei welchem uncommitete Daten gelesen und verarbeitet werden.

d) Gehalt von P1 kann entweder 600 oder 650 betragen. T1 überschreibt die Änderungen von T2. T2 jedoch kann die uncommiteten Änderungen jedoch als Dirty Read lesen und darauf aufbauend Daten ändern.

## Erklärung:

Serializable: Then both transactions try to commit. If either transaction were running at the Repeatable Read isolation level, both would be allowed to commit; but since there is no serial order of execution consistent with the result, using Serializable transactions will allow one transaction to commit and will roll the other back with this message.

Read Committed: Read Committed is the default isolation level in PostgreSQL. When a transaction uses this isolation level, a SELECT query (without a FOR UPDATE/SHARE clause) sees only data committed before the query began; it never sees either uncommitted data or changes committed during query execution by concurrent transactions. In effect, a SELECT query sees a snapshot of the database as of the instant the query begins to run.

Read Uncommited: The simplest explanation of the dirty read is the state of reading uncommitted data. In this circumstance, we are not sure about the consistency of the data that is read because we don’t know the result of the open transaction(s). After reading the uncommitted data, the open transaction can be completed with rollback.